import express from "express";

export const router = express.Router();

export default { router };

// Configurar primer ruta
router.get("/", (req, res) => {
  res.render("index", { titulo: "PreExamen C2 - Tirado Javier" });
});

router.get("/pago", (req, res) => {
  const params = {
    titulo: "PreExamen C2 - Tirado Javier",
    numero: req.query.numero,
    nombre: req.query.nombre,
    domicilio: req.query.domicilio,
    servicio: req.query.servicio,
    kwConsumidos: req.query.kwConsumidos,
    isPost: false,
  };
  res.render("pago", params);
});

router.post("/pago", (req, res) => {
  const precios = [1.08, 2.5, 3.0];

  const { numero, nombre, domicilio, servicio, kwConsumidos } = req.body;
  if(kwConsumidos <= 0){
    res.redirect('/pago');
  }else{
    const precioKw = precios[servicio * 1];
    const tipoDeServicio = servicio == 0 ? 'Domestico' :  servicio == 1 ? 'Comercial' : 'Industrial';
    const subtotal = precioKw * kwConsumidos;
  
    // Calcular el descuento
    let descuento = 0;
    if (kwConsumidos <= 1000) {
      descuento = 0.1;
    } else if (kwConsumidos > 1000 && kwConsumidos <= 10000) {
      descuento = 0.2;
    } else {
      descuento = 0.5;
    }
    // Calcular el impuesto
    const impuesto = 0.16 * subtotal;
  
    // Aplicar el descuento al subtotal
    const descuentoAplicado = subtotal * descuento;
    const subtotalConDescuento = subtotal - descuentoAplicado;
    
    // Calcular el total a pagar
    const total = subtotalConDescuento + impuesto;
  
    // Formatear los resultados con dos decimales
    const params = {
      titulo: "PreExamen C2 - Tirado Javier",
      numero,
      nombre,
      domicilio,
      servicio: tipoDeServicio,
      kwConsumidos,
      precioKw: precioKw.toFixed(2),
      subtotal: subtotal.toFixed(2),
      descuento: descuentoAplicado.toFixed(2),
      subtotalConDescuento: subtotalConDescuento.toFixed(2),
      impuesto: impuesto.toFixed(2),
      total: total.toFixed(2),
      isPost: true,
    };
    console.log(params);
    res.render("pago", params);
  }
});

router.get("/contacto", (req, res) => {
    res.render("contacto", { titulo: "PreExamen C2 - Tirado Javier" });
});
